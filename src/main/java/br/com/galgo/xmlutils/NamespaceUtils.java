package br.com.galgo.xmlutils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.TreeWalker;

import javax.xml.XMLConstants;

/**
 * Created by valdemar.arantes on 04/12/2014.
 */
public class NamespaceUtils {
    public static final String DEFAULT_NAMESPACE = "DEFAULT_NAMESPACE";
    private static final String XMLNAMESPACE = "xmlns";
    private static final String XMLNAMESPACE_COLLON = "xmlns:";
    private static final Logger log = LoggerFactory.getLogger(NamespaceUtils.class);


    /**
     * Recupera o prefixo de Namespace no escopo de um elemento do XML. Se o namespace for o Default,
     * é retornada a String "DEFAULT_NAMESPACE".
     *
     * @param el Elemento XML
     * @param ns Namespace cujo prefixo se deseja saber
     * @return Prefixo do Namespace
     */
    public static String getPrefix(Element el, String ns) {
        if (el == null) {
            throw new IllegalArgumentException("Argumento el n\u00e3o pode ser nulo");
        }
        log.debug("getPrefix({}), {}", el.getTagName(), ns);

        String pref = el.lookupPrefix(ns);
        if (pref != null) {
            return pref;
        } else {
            if (el.isDefaultNamespace(ns)) {
                log.debug("{} \u00e9 o Namespace default", ns);
                return DEFAULT_NAMESPACE;
            } else {
                final Node parentNode = el.getParentNode();
                if (parentNode == null || !(parentNode instanceof Element)) {
                    log.debug("Namespace {} não está definido no escopo da tag", ns);
                    return null;
                }
                ;
                return getPrefix((Element) parentNode, ns);
            }
        }
        /*
        log.debug("el.lookupPrefix(ns)={}", pref);
        NamedNodeMap atts = el.getAttributes();
        log.debug("{} atributo(s) encontrado(s)", atts.getLength());
        for (int i = 0; i < atts.getLength(); i++) {
            Node node = atts.item(i);
            String name = node.getNodeName();
            log.debug("node: ={name:{}, value:{}}", name, node.getNodeValue());
            if (ns.equals(node.getNodeValue())
                    && (name != null && (XMLNAMESPACE.equals(name) || name.startsWith(XMLNAMESPACE + ":")))) {
                return node.getLocalName();
            }
        }
        return null;
        */
    }

    public static void changeElementNamespace(Element elem, String namespace, String prefix) {


        log.debug("Alterando a tag do elemento {}", elem.getTagName());
        final Node parentNode = elem.getParentNode();


        // Criando um novo elemento com o novo namespace e prefixo
        Element newElement = parentNode.getOwnerDocument().createElementNS(namespace, elem.getNodeName());
        newElement.setPrefix(prefix);

        // Copiando todos os filhos do elemento original para o novo elemento criado acima
        NodeList list = elem.getChildNodes();
        while (list.getLength() != 0) {
            newElement.appendChild(list.item(0));
        }

        // Substituindo o elemento original pelo novo
        parentNode.replaceChild(newElement, elem);
        log.debug("Tag alterada com sucesso:");
        log.debug(XmlUtils.toString(parentNode));
    }

    public static void changeTreeNamespace(Element observacoes, String namespaceURI, String prefix) {
        setNamespace(observacoes, namespaceURI, prefix);
        TreeWalker obsWalker = XmlUtils.getTreeWalker(observacoes);
        changeTreeNamespace(obsWalker, namespaceURI);
    }

    public static void changeTreeNamespace(TreeWalker walker, String namespaceURI) {
        Node thisNode = walker.getCurrentNode();
        Document doc = thisNode.getOwnerDocument();
        while (thisNode != null) {
            if (thisNode instanceof Element) {
                doc.renameNode(thisNode, namespaceURI, thisNode.getNodeName());
                log.debug("Namespace do nó alterado:[{}]:{}", thisNode.getNamespaceURI(), thisNode.getNodeName());
            }
            thisNode = walker.nextNode();
        }
    }

    /**
     * Inclui a definição de um namespace em um elemento XML caso este namespace ainda não esteja definido
     *
     * @param elem
     * @param namespace
     * @param prefix
     * @return
     */
    public static String setNamespace(Element elem, String namespace, String prefix) {
        String pre = getPrefix(elem, namespace);
        if (pre != null) {
            return pre;
        }
        String xmlns;
        if (prefix == null || prefix.trim().equals("")) {
            xmlns = XMLNAMESPACE;
        } else {
            xmlns = XMLNAMESPACE_COLLON;
        }
        elem.setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, xmlns + prefix, namespace);
        return prefix;
    }

}
