package br.com.galgo.xmlutils;

import org.omg.CORBA.portable.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.TreeWalker;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.util.List;
import java.util.Locale;

/**
 * Created by valdemar.arantes on 03/12/2014.
 */
public class XmlUtils {
    private static final Logger log = LoggerFactory.getLogger(XmlUtils.class);
    private static final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    static {
        factory.setNamespaceAware(true);
    }

    /**
     * Monta um DOM a partir da URL do arquivo XML
     *
     * @param xmlUrl
     * @return Objeto DOM do XML
     * @throws ApplicationException
     */
    public static Document buildXml(URL xmlUrl) throws IOException, SAXException, ParserConfigurationException {
        if (xmlUrl == null) {
            throw new IllegalArgumentException("xmlUrl não pode ser nulo");
        }

        DocumentBuilder builder;
        builder = factory.newDocumentBuilder();
        return builder.parse(xmlUrl.openStream());
    }

    /**
     * Monta um DOM a partir da URL do arquivo XML
     *
     * @param xmlFile
     * @return Objeto DOM do XML
     * @throws ApplicationException
     */
    public static Document buildXml(File xmlFile) throws IOException, SAXException, ParserConfigurationException {
        if (xmlFile == null) {
            throw new IllegalArgumentException("xmlUrl não pode ser nulo");
        }

        DocumentBuilder builder;
        builder = factory.newDocumentBuilder();
        return builder.parse(xmlFile);
    }

    /**
     * Retorna uma inst&acirc;ncia de TreeWalker que percorre todos os elementos a partir do n&oacute; raiz
     *
     * @param root N&oacute; raiz
     * @return Inst&acirc;ncia de TreeWalker
     */
    public static final TreeWalker getTreeWalker(Node root) {
        int whattoshow = NodeFilter.SHOW_ALL;
        NodeFilter nodefilter = null;
        boolean expandreferences = false;

        DocumentTraversal traversal = (DocumentTraversal) root.getOwnerDocument();
        TreeWalker walker = traversal.createTreeWalker(root, whattoshow, nodefilter, expandreferences);
        return walker;
    }

    /**
     * Gera uma String com todos os nomes dos elementos encontrados percorrendo o par&acirc;metro walker. O formato
     * de sa&iacute;da &eacute; NamespaceURI:LocalName\n
     *
     * @param walker
     * @return String com a lista dos elementos da árvore
     */
    public static final String printElements(TreeWalker walker) {
        Node thisNode = walker.getCurrentNode();
        StringBuilder buff = new StringBuilder();
        while (thisNode != null) {
            if (thisNode instanceof Element) {
                Element elem = (Element) thisNode;
                buff.append(thisNode.getNamespaceURI()).append(":").append(thisNode.getNodeName()).append("\n");
            }
            thisNode = walker.nextNode();
        }
        return buff.toString();
    }

    /**
     * Retorna a representa&ccedil;&atilde;o String do Node de um XML DOM
     *
     * @param node
     * @param pretty Indica se a String deve ser formatada (true) ou n&atilde;o. Default = false
     * @return Representa&ccedil;&atilde;o String UTF-8 do Node
     * @throws javax.xml.transform.TransformerException
     */
    public static String toString(Node node, boolean pretty) {
        DOMSource domSource = new DOMSource(node);
        Transformer transformer = null;
        try {
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            if (pretty) {
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            }

            StringWriter sw = new StringWriter();
            StreamResult sr = new StreamResult(sw);
            transformer.transform(domSource, sr);
            return sw.toString();
        } catch (TransformerException e) {
            log.error(null, e);
            return e.toString();
        }
    }

    /**
     * Retorna a representa&ccedil;&atilde;o String formatada do Node de um XML DOM
     *
     * @param node
     * @return Representa&ccedil;&atilde;o String UTF-8 do Node
     * @throws javax.xml.transform.TransformerException
     */
    public static String toString(Node node) {
        return toString(node, false);
    }

    /**
     * Faz a validação do XML utilizando os schemas definidos em vários XSDs. O Locale é fixado para en.
     *
     * @param doc DOM dom XML a ser validado
     * @param xsdURLs Lista das URLs dos arquivos XSDs
     *
     * @return Lista de Strings com os erros
     *
     * @throws SAXException
     * @throws IOException
     */
    public static List<String> validate(Document doc, URL... xsdURLs) throws SAXException, IOException {
        /**********************************************************************************
         *                                     Validações
         **********************************************************************************/
        if (doc == null) {
            throw new IllegalArgumentException("Parâmetro doc não pode ser nulo");
        }

        if (xsdURLs == null || xsdURLs.length == 0) {
            throw new IllegalArgumentException("O parâmetro xsdURLs com lista das URLs dos XSDs não pode estar vazia");
        }
        /****************************** Fim das Validações ****************************************/

        Source[] schemas = new Source[xsdURLs.length];
        for (int i = 0; i < xsdURLs.length; i++) {
            schemas[i] = new StreamSource(xsdURLs[i].toExternalForm());
        }

        SchemaFactory schFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = schFactory.newSchema(schemas);

        Validator validator = schema.newValidator();
        ToStringListErrorHandler errHandler = new ToStringListErrorHandler(validator);

        // Forçando o uso do inglês para a geração das mensagens de erro
        Locale defaultLocale = Locale.getDefault();
        Locale.setDefault(Locale.ENGLISH);

        // Validação
        validator.validate(new DOMSource(doc));

        // Retornando o local para o original
        Locale.setDefault(defaultLocale);

        return errHandler.getErrors();
    }
}
