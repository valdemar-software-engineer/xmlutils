package br.com.galgo.xmlutils;

import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.validation.Validator;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by valdemar.arantes on 12/12/2014.
 */
public class ToStringListErrorHandler extends DefaultHandler {

    private final List<String> errors = new ArrayList<>();
    private final Validator validator;

    public ToStringListErrorHandler(Validator validator) {
        this.validator = validator;
        validator.setErrorHandler(this);
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        printInfo(e);
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        printInfo(e);
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        printInfo(e);
    }

    public List<String> getErrors() {
        return errors;
    }

    private void printInfo(SAXParseException e) {
        try {
            System.out.println("Erro no elemento" +
                    validator.getProperty("http://apache.org/xml/properties/dom/current-element-node"));
        } catch (SAXNotRecognizedException e1) {
            e.printStackTrace();
        } catch (SAXNotSupportedException e1) {
            e.printStackTrace();
        }
        errors.add("Linha " + e.getLineNumber() + "; Coluna " + e.getColumnNumber()
                + "; Descrição: " + e.getMessage());
    }
}

