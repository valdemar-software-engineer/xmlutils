package br.com.galgo.xmlutils;

import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by valdemar.arantes on 04/12/2014.
 */
public class NamespaceUtilsTest {
    private static final Logger log = LoggerFactory.getLogger(NamespaceUtilsTest.class);
    private static final String INÍCIO_DO_TESTE = "*********************** Início do teste *****************";
    private static final String FIM_DO_TESTE = "*********************** Fim do teste *****************";
    private static Document doc;
    private static Element root;
    private static Element teste;

    @BeforeTest
    public static void iniTest() throws ParserConfigurationException {
        doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        root = doc.createElementNS("namespace.de.teste", "doc");
        root.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:aaa", "aaa.namespace");
        root.setPrefix("nst");
        NamespaceUtils.setNamespace(root, "bbb.namespace", "bbb");
        NamespaceUtils.setNamespace(root, "ccc.namespace", "");
        doc.appendChild(root);
        root.appendChild(doc.createElement("teste"));
        teste = (Element) root.getChildNodes().item(0);
        log.debug("doc=\n{}", XmlUtils.toString(doc, true));
    }

    @Test
    public void getPrefixTest() {
        log.info(INÍCIO_DO_TESTE);
        try {
            Assertions.assertThat(prefixTest("namespace.de.teste")[0]).isEqualTo("nst");
            Assertions.assertThat(prefixTest("namespace.de.teste")[1]).isEqualTo("nst");
            Assertions.assertThat(prefixTest("aaa.namespace")[0]).isEqualTo("aaa");
            Assertions.assertThat(prefixTest("aaa.namespace")[1]).isEqualTo("aaa");
            Assertions.assertThat(prefixTest("bbb.namespace")[0]).isEqualTo("bbb");
            Assertions.assertThat(prefixTest("bbb.namespace")[1]).isEqualTo("bbb");
            Assertions.assertThat(prefixTest("ccc.namespace")[0]).isEqualTo(NamespaceUtils.DEFAULT_NAMESPACE);
            Assertions.assertThat(prefixTest("ccc.namespace")[1]).isEqualTo(NamespaceUtils.DEFAULT_NAMESPACE);
            Assertions.assertThat(prefixTest("xxx.namespace")[0]).isNull();
            Assertions.assertThat(prefixTest("xxx.namespace")[1]).isNull();
        } finally {
            log.info(FIM_DO_TESTE);
        }
    }

    private static String[] prefixTest(String ns) {
        String[] ret = new String[2];
        String prefix = NamespaceUtils.getPrefix(root, ns);
        log.debug("prefix={}", prefix);
        ret[0] = prefix;
        prefix = NamespaceUtils.getPrefix(teste, ns);
        log.debug("prefix={}", prefix);
        ret[1] = prefix;
        return ret;
    }
}
