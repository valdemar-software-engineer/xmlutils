package br.com.galgo.xmlutils;

import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.net.URL;
import java.util.List;

/**
 * Created by valdemar.arantes on 04/12/2014.
 */
public class XmlUtilsTest {

    private static final Logger log = LoggerFactory.getLogger(XmlUtilsTest.class);
    private static final String INÍCIO_DO_TESTE = "*********************** Início do teste *****************";
    private static final String FIM_DO_TESTE = "*********************** Fim do teste *****************";
    private static Document doc;

    @BeforeTest
    public static void iniTest() throws ParserConfigurationException {
        doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        final Element root = doc.createElementNS("namespace.de.teste", "doc");
        doc.appendChild(root);
        root.appendChild(doc.createElement("teste"));
    }

    @Test
    public void toStringTest() {
        log.info(INÍCIO_DO_TESTE);
        try {
            String strDoc = XmlUtils.toString(doc);
            Assertions.assertThat(strDoc).
                    isEqualToIgnoringCase("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>" +
                            "<doc xmlns=\"namespace.de.teste\"><teste/></doc>");
        } finally {
            log.info(FIM_DO_TESTE);
        }
    }

    @Test
    public void toStringPretty() {
        log.info(INÍCIO_DO_TESTE);
        try {
            String strDoc = XmlUtils.toString(doc, true);
            Assertions.assertThat(strDoc).
                    containsIgnoringCase("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>").
                    containsIgnoringCase("    <teste/>");
        } finally {
            log.info(FIM_DO_TESTE);
        }
    }

    @Test
    public void validateTest() {
        log.info(INÍCIO_DO_TESTE);
        try {
            URL xmlUrl = this.getClass().getResource("/escola_com_tag_invalida.xml");
            File xmlFile = new File(xmlUrl.toURI());
            URL[] xsdURLs = new URL[2];
            xsdURLs[0] = this.getClass().getResource("/escola.xsd");
            xsdURLs[1] = this.getClass().getResource("/observacoes.xsd");

            Document doc = XmlUtils.buildXml(xmlFile);
            List<String> errors = XmlUtils.validate(doc, xsdURLs);
            StringBuilder buff = new StringBuilder();
            for (String error : errors) {
                buff.append(error).append("\n");
            }
            log.debug("errors:\n{}", buff);

            Assertions.assertThat(errors).hasSize(2);
            Assertions.assertThat(errors.get(0)).containsIgnoringCase("Invalid content").containsIgnoringCase("tag_invalida_1");
            Assertions.assertThat(errors.get(1)).containsIgnoringCase("Invalid content").containsIgnoringCase("tag_invalida_2");

        } catch (Exception e) {
            log.error(e.toString(), e);
            Assertions.fail(e.toString());
        } finally {
            log.info(FIM_DO_TESTE);
        }
    }
}
